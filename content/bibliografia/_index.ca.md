---
title: "Bibliografia"
weight: 80
chapter: false
---

Forcada, M. L., Ginestí-Rosell, M., Nordfalk, J., O’Regan, J., Ortiz-Rojas, S., Pérez-Ortiz, J. A., . . . Tyers, F. M. (2011). Apertium: A free/open-source platform for rule-based machine translation. *Machine Translation*, 25(2), 127 - 144.

Free Software Foundation. (s.d.). *What is free software?* Recuperat el 25 març de 2019, de https://www.gnu.org/philosophy/free-sw.en.html

Hutchins, W. J. i Somers, H. L. (1992). *An introduction to machine translation.* London [etc.]: Academic Press.

&nbsp;

Imatge sobre el funcionament intern d'Apertium: http://wiki.apertium.org/wiki/Apertium_system_architecture
