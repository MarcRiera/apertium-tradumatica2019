---
title: "Módulos"
weight: 22
disableToc: true
---

Gracias a la modularidat de Apertium, la cadena se puede adaptar a las necesidades de cada par, en función de las operaciones necesarias para traducir. De todos modos, los módulos básicos (presentes en todos los pares) son los siguientes:

1. **Desformateador:** separa el texto de la lengua de origen del formato, que queda encapsulado.

2. **Analizador morfológico:** divide el texto en unidades léxicas y proporciona todos los análisis posibles para cada una. El análisis incluye la forma interna de la unidad en el diccionario y la información morfológica.

3. **Desambiguador léxico:** elige el análisis correcto según un modelo estadístico cuando una unidad léxica tiene más de un análisis posible.

4. **Módulo de transferencia léxica:** consultando un diccionario bilingüe, proporciona uno o más equivalentes en la lengua de llegada para cada unidad léxica.

5. **Módulo de transferencia estructural:** aplica cambios estructurales (cambios de orden, concordancia, sustituciones, etc.) a patrones de unidades léxicas. En pares de lenguas próximas, como el castellano–catalán, los cambios se aplican en una fase, mientras que en pares de lenguas más lejanas se hace en más pasos, como en el caso del par inglés–catalán, que usa tres.

6. **Generador morfológico:** convierte las formas internas de las unidades léxicas en formas finales (superficiales).

7. **Posgenerador:** aplica modificaciones ortográficas, como las apostrofaciones y las contracciones.

8. **Reformateador:** recupera la información de formato del desformateador y la inserta en el texto traducido.

Otros módulos creados posteriormente y que se usan solo en algunos pares son:

- **Módulo de gramática de restricciones:** realiza operaciones de desambiguación a partir de un conjunto de reglas, como complemento del desambiguador estadístico.

- **Módulo de selección léxica:** para las unidades léxicas con más de un equivalente en la lengua de llegada, elige entre las diversas opciones a partir de reglas de contexto.

- **Módulo de multipalabras separables:** aplica cambios de orden sobre patrones de la lengua de origen para permitir el reconocimiento correcto de las multipalabras separables (como los «phrasal verbs» del inglés)
