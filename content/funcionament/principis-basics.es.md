---
title: "Principios básicos"
weight: 21
disableToc: true
---

Apertium es un sistema de traducción automática basada en reglas de transferencia superficial.

Está formado por una serie de módulos conectados en cadena. Estos módulos no están diseñados para una combinación de lenguas en concreto; los datos lingüísticos de los pares se almacenan aparte, de forma que es posible crear pares nuevos sin tener que modificar los módulos en sí.

![Apertium pipeline](/images/Apertium_system_architecture.png)

Los módulos se comunican mediante una «tubería» de texto que al principio contiene el texto original. Cada módulo lo altera haciendo la tarea concreta para la que ha sido programado hasta que después del último módulo el texto se encuentra en la lengua de llegada. Ejemplo de traducción de la frase «The blue house» del inglés al catalán («La casa blava»):

```xml
1. The blue house
2. ^The/the<det><def><sp>$ ^blue/blue<adj><sint>/blue<n><sg>$ ^house/house<n><sg>/house<vblex><inf>/house<vblex><pres>/house<vblex><imp>$ 
3. ^The/The<det><def><sp>$ ^blue/blue<adj><sint>$ ^house/house<n><sg>$ 
4. ^The<det><def><sp>$ ^blue<adj><sint>$ ^house<n><sg>$ 
5. ^The<det><def><sp>/El<det><def><GD><ND>$ ^blue<adj><sint>/blau<adj>$ ^house<n><sg>/casa<n><f><sg>/cambra<n><f><sg>/càmera<n><f><sg>$ 
6. ^Det_nom_adj<SN><DET><f><sg>{^el<det><def><3><4>$ ^casa<n><3><4>$ ^blau<adj><3><4>$}$ 
7. ^El<det><def><f><sg>$ ^casa<n><f><sg>$ ^blau<adj><f><sg>$ 
8. ~La casa blava 
9. La casa blava 
```

En cuanto a los datos lingüísticos, cada par de lenguas está formado por tres paquetes: dos paquetes monolingües (compartidos con otros pares, uno para la lengua de origen y el otro para la lengua de llegada) y un paquete bilingüe (específico del par de lenguas).
