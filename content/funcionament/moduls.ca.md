---
title: "Mòduls"
weight: 22
disableToc: true
---

Gràcies a la modularitat d’Apertium, la cadena es pot adaptar a les necessitats de cada parell, en funció de les operacions necessàries per a traduir. De totes maneres, els mòduls bàsics (presents a tots els parells) són els següents:

1. **Desformatador:** separa el text de la llengua d’origen del format, que queda encapsulat.

2. **Analitzador morfològic:** divideix el text en unitats lèxiques i proporciona totes les anàlisis possibles per a cadascuna. L’anàlisi inclou la forma interna de la unitat al diccionari i la informació morfològica.

3. **Desambiguador lèxic:** tria l’anàlisi correcta segons un model estadístic quan una unitat lèxica té més d’una anàlisi possible.

4. **Mòdul de transferència lèxica:** consultant un diccionari bilingüe, proporciona un o més equivalents en la llengua d’arribada per a cada unitat lèxica.

5. **Mòdul de transferència estructural:** aplica canvis estructurals (canvis d’ordre, concordança, substitucions, etc.) a patrons d’unitats lèxiques. En parells de llengües properes, com el castellà–català, els canvis s’apliquen en una fase, mentre que en parells de llengües més llunyanes es fa en més passos, com en el cas del parell anglès–català, que en fa servir tres.

6. **Generador morfològic:** converteix les formes internes de les unitats lèxiques en formes finals (superficials).

7. **Postgenerador:** aplica modificacions ortogràfiques, com les apostrofacions i les contraccions.

8. **Reformatador:** recupera la informació de format del desformatador i la insereix al text traduït.

Altres mòduls creats posteriorment i que es fan servir només en alguns parells són:

- **Mòdul de gramàtica de restriccions:** realitza operacions de desambiguació a partir d’un conjunt de regles, com a complement del desambiguador estadístic.

- **Mòdul de selecció lèxica:** per a les unitats lèxiques amb més d’un equivalent en la llengua d’arribada, tria entre les diverses opcions a partir de regles de context.

- **Mòdul de multiparaules separables:** aplica canvis d’ordre sobre patrons de la llengua d’origen per tal de permetre el reconeixement correcte de les multiparaules separables (com els «phrasal verbs» de l’anglès)
