---
title: "Comunitat"
weight: 23
disableToc: true
---

Apertium, com a projecte lliure de codi obert, permet que qualsevol persona pugui fer servir la TA i a més, si té els coneixements necessaris, pugui modificar qualsevol part del programa i millorar-lo.

## Codi font

El codi font es troba disponible a GitHub, un servei d'emmagatzematge per a control de versions. Cada mòdul, llengua, parell de llengües o eina disposa d'un repositori propi, fet que permet organitzar fàcilment els equips de treball, manipular els fitxers i evitar riscos innecessaris. La major part de la feina per part de la comunitat té lloc als repositoris de dades lingüístiques, que es poden classificar en cinc categories (antigament carpetes reals):

- **apertium-languages:** paquets monolingües.

- **apertium-trunk:** paquets bilingües que han arribat a un cert grau de maduresa i estabilitat i que s'han publicat oficialment.

- **apertium-staging:** paquets bilingües que han tingut un desenvolupament extens però que encara no estan preparats per a publicar-se.

- **apertium-nursery:** paquets bilingües que es poden compilar però que no han rebut un desenvolupament extens.

- **apertium-incubator:** dades de qualsevol tipus que poden ser útils però que de moment no s'han fet servir.

Tota aquesta classificació es pot consultar fàcilment [aquí](https://apertium.github.io/apertium-on-github/source-browser.html).

---

Com passa en qualsevol projecte de codi obert, el codi principal només el pot modificar un grup determinat de persones. Si algú que no té permisos vol col·laborar en un repositori, té disponible una funció pròpia de Git anomenada «pull request», que és una petició per afegir canvis. L'usuari crea una bifurcació («fork») del codi en un repositori personal (on sí que té permisos per editar), hi afegeix els canvis que vol proposar i finalment fa la petició per incorporar el seu nou codi al repositori oficial. [Exemple](https://github.com/apertium/apertium-spa-cat/pull/2).

Aquest sistema de contribució és pràctic per a canvis menors i no recurrents. Ara bé, si un usuari vol col·laborar de manera habitual, pot sol·licitar permisos mitjançant les llistes de correu.

## Llistes de correu

Existeixen diverses llistes de correu electrònic per fer preguntes, propostes i anuncis, o per debatre qualsevol qüestió del projecte. La principal és apertium-stuff, però n'hi ha d'altres per a llengües específiques. Hi ha més informació en [aquesta pàgina](http://wiki.apertium.org/wiki/Contact).

## Xat

Apertium també disposa d'una sala de xat a IRC, on és fàcil parlar en temps real amb la resta de col·laboradors del projecte. La sala té el nom #apertium i es troba a irc.freenode.net.

![apertium-irc](/images/Apertium-IRC.png)
