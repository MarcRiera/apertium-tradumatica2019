---
title: "Comunidad"
weight: 23
disableToc: true
---

Apertium, como proyecto libre de código abierto, permite que cualquier persona pueda usar la TA y además, si tiene los conocimientos necesarios, pueda modificar cualquier parte del programa y mejorarlo.

## Código fuente

El código fuente se encuentra disponible en GitHub, un servicio de almacenamiento para control de versiones. Cada módulo, lengua, par de lenguas o herramienta dispone de un repositorio propio, lo que permite organizar fácilmente los equipos de trabajo, manipular los archivos y evitar riesgos innecesarios. La mayor parte del trabajo por parte de la comunidad tiene lugar en los repositorios de datos lingüísticos, que se pueden clasificar en cinco categorías (antiguamente carpetas reales):

- **apertium-languages:** paquetes monolingües.

- **apertium-trunk:** paquetes bilingües que han llegado a un cierto grado de madurez y estabilidad y que se han publicado oficialmente.

- **apertium-staging:** paquetes bilingües que han tenido un desarrollo extenso pero que todavía no están preparados para publicarse.

- **apertium-nursery:** paquetes bilingües que se pueden compilar pero que no han recibido un desarrollo extenso.

- **apertium-incubator:** datos de cualquier tipo que pueden ser útiles pero que de momento no se han usado.

Toda esta clasificación se puede consultar fácilmente [aquí](https://apertium.github.io/apertium-on-github/source-browser.html).

---

Como pasa en cualquier proyecto de código abierto, el código principal solo lo puede modificar un grupo determinado de personas. Si alguien que no tiene permisos quiere colaborar en un repositorio, tiene disponible una función propia de Git llamada «pull request», que es una petición para añadir cambios. El usuario crea una bifurcación («fork») del código en un repositorio personal (donde sí tiene permisos para editar), añade los cambios que quiere proponer y finalmente hace la petición para incorporar su nuevo código al repositorio oficial. [Ejemplo](https://github.com/apertium/apertium-spa-cat/pull/2).

Este sistema de contribución es práctico para cambios menores y no recurrentes. Ahora bien, si un usuario quiere colaborar de manera habitual, puede solicitar permisos mediante las listas de correo.

## Listas de correo

Existen varias listas de correo electrónico para hacer preguntas, propuestas y anuncios, o para debatir cualquier cuestión del proyecto. La principal es apertium-stuff, pero hay otras para lenguas específicas. Hay más información en [esta página](http://wiki.apertium.org/wiki/contact).

## Chat

Apertium también dispone de una sala de chat en IRC, donde es fácil hablar en tiempo real con el resto de colaboradores del proyecto. La sala tiene el nombre #apertium y se encuentra en irc.freenode.net.

![apertium-irc](/images/Apertium-IRC.png)
