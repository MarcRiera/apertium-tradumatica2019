---
title: "Inici"
chapter: true
---
# Traducció automàtica basada en regles des del punt de vista comunitari

Et dono la benvinguda al lloc web de la sessió tradumàtica de 2019 sobre traducció automàtica basada en regles (TABR). Aquí trobaràs informació general sobre la TABR, Apertium, com funciona i com hi pots contribuir.

Navega per la guia amb el menú lateral esquerre i les fletxes a banda i banda del contingut.

&nbsp;

Pots baixar-te la presentació de la sessió des d'[aquí](/files/Tradumatica-Apertium2019.pdf).
