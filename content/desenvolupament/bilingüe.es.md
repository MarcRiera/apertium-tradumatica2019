---
title: "Diccionario bilingüe"
weight: 34
---

El diccionario bilingüe contiene una lista de equivalencias entre entradas de las dos lenguas que forman el par. Se estructura en dos columnas: a la izquierda está la forma interna con etiquetas de la entrada de la primera lengua, y a la derecha está la forma interna con etiquetas equivalente de la segunda lengua.

Ejemplos de entradas bilingües entre el inglés y el catalán:

```xml
<e><p><l>house<s n="n"/><s n="sg"/></l><r>casa<s n="n"/><s n="f"/><s n="sg"/></r></p></e>
<e><p><l>house<s n="n"/><s n="pl"/></l><r>casa<s n="n"/><s n="f"/><s n="pl"/></r></p></e>
```

A pesar de que el ejemplo es correcto y funcional, definir las equivalencias entre entradas léxicas muy complejas (como los verbos) sería demasiado pesado. Por suerte, podemos simplificarlo obviando las etiquetas que se repiten a ambos lados: Apertium no transfiere las etiquetas especificadas en el diccionario y transfiere automáticamente de un lado al otro cualquier etiqueta extra:


```xml
<e><p><l>house<s n="n"/></l><r>casa<s n="n"/><s n="f"/></r></p></e>
``` 

En este caso, la entrada serviría tanto para «house\<n\>\<sg\>» como para «house\<n\>\<pl\>».

Del mismo modo que con los diccionarios monolingües, es posible definir paradigmas:

```xml
<pardef n="house-casa__n">
    <e><p><l><s n="n"/></l><r><s n="n"/><s n="f"/></r></p></e>
</pardef>

<e><p><l>house</l><r>casa</r></p><par n="house-casa__n"/></e>
```

Finalmente, está la opción de definir entradas no bidireccionales. En el caso de los diccionarios bilingües, «LR» (left-to-right) indica que una entrada solo funcionará de izquierda a derecha, y «RL» indica que solo funcionará de derecha a izquierda. Ejemplo:

```xml
<e r="LR"><p><l>house<s n="n"/></l><r>llar<s n="n"/><s n="f"/></r></p></e>
<e r="RL"><p><l>dream<s n="vblex"/></l><r>somniar<s n="vblex"/></r></p></e>
```

Esto es muy útil cuando una de las dos lenguas tiene género y la otra no, como pasa con el inglés y el catalán. Ejemplo (la etiqueta «\<GD\>» indica que Apertium tiene que escoger entre uno de los dos géneros en función de la información que tenga):

```xml
<e r="LR"><p><l>president<s n="n"/></l><r>president<s n="n"/><s n="GD"/></r></p></e>
<e r="RL"><p><l>president<s n="n"/></l><r>president<s n="n"/><s n="m"/></r></p></e>
<e r="RL"><p><l>president<s n="n"/></l><r>president<s n="n"/><s n="f"/></r></p></e>
```

