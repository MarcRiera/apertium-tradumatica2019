---
title: "Diccionario monolingüe"
weight: 33
---

Los diccionarios monolingües contienen una lista de entradas léxicas con etiquetas que proporcionan información morfológica. Se estructuran en dos columnas: a la izquierda está la forma superficial de la entrada (con flexión y sin etiquetas), y a la derecha está la forma interna (la forma base con etiquetas). La forma interna sirve para agrupar en una misma entrada diferentes formas superficiales, como las conjugaciones verbales o las diferentes formas de los adjetivos.

Ejemplos de entradas léxicas en catalán («casa», sustantivo femenino):

```xml
<e><p><l>casa</l><r>casa<s n="n"/><s n="f"/><s n="sg"/></r></p></e>
<e><p><l>cases</l><r>casa<s n="n"/><s n="f"/><s n="pl"/></r></p></e>
```
La primera entrada del ejemplo anterior indica que la forma superficial «casa» equivale a la forma interna «casa<n><f><sg>» (nombre femenino singular). La segunda indica que la forma superficial «casas» equivale a la forma interna «casa<n><f><pl>» (nombre femenino plural).

A pesar de que este ejemplo es correcto y funcional, solo para «casa» hacen falta dos entradas: una para la forma en singular y otra para la forma en plural. Esto es poco eficiente (con conjugaciones verbales el diccionario se complicaría demasiado), por lo que habitualmente se recurre al uso de paradigmas.

Un paradigma es una entrada léxica que sirve de modelo para otras. «Taula», por ejemplo, hace el plural del mismo modo que «casa» (cambia -a por -es). Por lo tanto, con el paradigma «casa» podemos obtener la flexión de cualquier sustantivo femenino similar. Ejemplo de paradigma y entradas léxicas que se basan en él:

```xml
<pardef n="cas/a__n">
    <e><p><l>a</l><r>a<s n="n"/><s n="f"/><s n="sg"/></r></p></e>
    <e><p><l>es</l><r>a<s n="n"/><s n="f"/><s n="pl"/></r></p></e>
</pardef>

<e lm="casa"><i>cas</i><par n="cas/a__n"/></e>
<e lm="taula"><i>taul</i><par n="cas/a__n"/></e>
```

Los paradigmas permiten ahorrar tiempo y espacio, y contribuyen sustancialmente a la buena organización interna de los diccionarios.

Finalmente, está la opción de definir entradas no bidireccionales. En el caso de los diccionarios monolingües, «LR» (left-to-right) indica que una entrada solo se puede analizar, y «RL» indica que solo se puede generar. Ejemplo:

```xml
<e r="LR" lm="somniar"><i>somni</i><par n="cant/ar__vblex"/></e>
```
