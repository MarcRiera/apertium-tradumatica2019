---
title: "Compilació"
weight: 31
---

Perquè Apertium pugui entendre les dades lingüístiques d'un parell, cal compilar-les al format intern del programa. Això s'ha de fer cada vegada que es modifiquen els diccionaris, o que es canvia qualsevol altre fitxer del parell.

La primera vegada que es compila un parell després de clonar els repositoris cal configurar-lo. Això s'aconsegueix obrint un terminal a cadascuna de les dues carpetes monolingües i executant-hi l'ordre següent:

```sh
./autogen.sh
```

Quan s'hagi acabat d'executar, es pot tancar el terminal i obrir-ne un de nou a la carpeta bilingüe. En aquest cas, l'ordre canvia lleugerament perquè cal indicar la ruta de les carpetes monolingües (exemple amb el parell castellà-català):

```sh
./autogen.sh --with-lang1=../apertium-spa --with-lang2=../apertium-cat
```

Fet això, cada vegada que es vulgui compilar el parell n'hi ha prou amb executar l'ordre següent a la carpeta bilingüe:

```sh
make langs
```
