---
title: "Previsualización"
weight: 32
---

Para trabajar en un par, resulta interesante poder comprobar el funcionamiento interno de Apertium en tiempo real. Esto es posible gracias al programa Apertium Viewer.

![apertium-viewer](/images/Apertium-Viewer.png)

La primera vez que se ejecuta el programa escanea las carpetas del ordenador para buscar pares de Apertium y añadirlos automáticamente. En cualquier momento se pueden añadir más desde el menú File > Load a language pair. Los archivos de par (modos) se encuentran en la carpeta «modes» dentro de la carpeta bilingüe.

<font color="red">AVISO:</font>Asegúrate de que la casilla «C++ version» en la parte superior izquierda del programa está marcada (y no «Java») para evitar errores inesperados.
