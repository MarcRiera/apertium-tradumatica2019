---
title: "Compilación"
weight: 31
---

Para que Apertium pueda entender los datos lingüísticos de un par, hay que compilarlos al formato interno del programa. Esto se tiene que hacer cada vez que se modifican los diccionarios, o que se cambia cualquier otro fichero del par.

La primera vez que se compila un par después de clonar los repositorios hay que configurarlo. Esto se consigue abriendo un terminal en cada una de las dos carpetas monolingües y ejecutando la orden siguiente:

```sh
./autogen.sh
```

Cuando se haya acabado de ejecutar, se puede cerrar el terminal y abrir uno de nuevo en la carpeta bilingüe. En este caso, la orden cambia ligeramente porque hay que indicar la ruta de las carpetas monolingües (ejemplo con el par castellano-catalán):

```sh
./autogen.sh --with-lang1=../apertium-spa --with-lang2=../apertium-cat
```

Hecho esto, cada vez que se quiera compilar el par es suficiente con ejecutar la orden siguiente en la carpeta bilingüe:

```sh
make langs
```
