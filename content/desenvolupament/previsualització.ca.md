---
title: "Previsualització"
weight: 32
---

Per treballar en un parell, resulta interessant poder comprovar el funcionament intern d'Apertium en temps real. Això és possible gràcies al programa Apertium Viewer.

![apertium-viewer](/images/Apertium-Viewer.png)

La primera vegada que s'executa el programa escaneja les carpetes de l'ordinador per buscar-hi parells d'Apertium i afegir-los automàticament. En qualsevol moment se'n poden afegir més des del menú File > Load a language pair. Els fitxers de parell (modes) es troben a la carpeta «modes» dins de la carpeta bilingüe.

<font color="red">AVÍS:</font> Assegura't que la casella «C++ version» a la part superior esquerra del programa està marcada (i no «Java») per evitar errors inesperats.
