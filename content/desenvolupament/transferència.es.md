---
title: "Reglas de transferencia estructural"
weight: 35
---

Las reglas de transferencia estructural definen acciones que se tienen que aplicar sobre las entradas léxicas cuando se cumplen unos parámetros determinados. Con estas reglas es posible alterar las etiquetas de las entradas, agrupar las entradas y cambiar el orden de ellas.

Por ejemplo: en inglés, los adjetivos siempre aparecen delante del nombre. En catalán, en cambio, la posición habitual de los adjetivos es detrás del nombre. Por lo tanto, a la hora de traducir un sintagma como «the blue house», se tendrá que invertir el orden de las dos últimas palabras. Además, en catalán los adjetivos y determinantes tienen género y número, por lo que también se tendrán que ajustar «el» y «blau».

```
1. The blue house (original)
2. El blau casa (transferencia léxica)
3. La casa blava (transferencia estructural)
```

Las reglas se escriben en lenguaje XML, igual que los diccionarios, pero tienen una estructura muy variada. Puedes ver algunos ejemplos aquí (en inglés): http://wiki.apertium.org/wiki/Transfer_rules_examples
