---
title: "Entrades noves als diccionaris"
weight: 51
---

En aquesta pràctica aprendràs a expandir els diccionaris d'Apertium perquè reconegui paraules que encara no coneix. La pràctica es basa en el parell castellà-català.

## Diccionari monolingüe

Afegeix les entrades següents al diccionari de castellà:

```xml
selfie
táper
postureo
posverdad
tradumática
narcopiso
heteropatriarcado
meme
gentrificación
cacerolada
```

Ara fes el mateix però en català:

```xml
selfie
tàper
postureig
postveritat
tradumática
narcopis
heteropatriarcat
mem
gentrificació
cassolada
```

## Diccionari bilingüe

A partir de les entrades que has afegit anteriorment, crea la llista d'equivalències al diccionari bilingüe del parell.

---

Quan hagis completat tots els passos, prova de compilar el parell i d'obrir-lo amb Apertium Viewer per veure els canvis.

