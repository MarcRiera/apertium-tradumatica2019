---
title: "Apertium: pràctica"
weight: 50
pre: "<b>5. </b>"
chapter: true
---
### Capítol 5

# Pràctica

Aprèn a afegir entrades als diccionaris d'Apertium i comprova els resultats en directe.
