---
title: "Apertium: práctica"
weight: 50
pre: "<b>5. </b>"
chapter: true
---
### Capítulo 5

# Práctica

Aprende a añadir entradas a los diccionarios de Apertium y comprueba los resultados en directo.
