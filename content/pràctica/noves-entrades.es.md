---
title: "Entradas nuevas en los diccionarioss"
weight: 51
---

En esta práctica aprenderás a expandir los diccionarios de Apertium para que reconozca palabras que todavía no conoce. La práctica se basa en el par de lenguas castellano-catalán.

## Diccionario monolingüe

Añade las entradas siguientes al diccionari de castellano:

```xml
selfie
táper
postureo
posverdad
tradumática
narcopiso
heteropatriarcado
meme
gentrificación
cacerolada
```

Ahora haz lo mismo pero en catalán:

```xml
selfie
tàper
postureig
postveritat
tradumática
narcopis
heteropatriarcat
mem
gentrificació
cassolada
```

## Diccionario bilingüe

A partir de las entradas que has añadido anteriormente, crea una lista de equivalencias en el diccionario bilingüe del par.

---

Cuando hayas completado todos los pasos, intenta compilar el par y ábrelo en Apertium Viewer para ver los cambios.

