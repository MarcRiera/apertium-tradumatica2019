---
title: "Inicio"
chapter: true
---
# Traducción automática basada en reglas desde el punto de vista comunitario

Te doy la bienvenida al sitio web de la sesión tradumática de 2019 sobre traducción automática basada en reglas (TABR). Aquí encontrarás información general sobre la TABR, Apertium, cómo funciona y cómo puedes contribuir.

Navega por la guía usando el menú lateral izquierdo y las flechas a ambos lados del contenido.

&nbsp;

Puedes descargar la presentación de la sessión desde [aquí](/files/Apertium-presentacio.pdf).
