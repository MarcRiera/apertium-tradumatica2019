---
title: "Dades lingüístiques"
weight: 24
disableToc: true
---

Per baixar les dades lingüístiques d'un parell d'Apertium només cal conèixer els URL del paquet bilingüe i dels dos paquets monolingües i tenir instal·lat Git a l'ordinador.

Per exemple, per baixar els parells castellà-català i anglès-català:

1. Obre un terminal en una carpeta de treball (per exemple, Documents/apertium).

2. Executa les ordres següents:
```sh
git clone https://github.com/apertium/apertium-spa-cat
git clone https://github.com/apertium/apertium-eng-cat
git clone https://github.com/apertium/apertium-spa
git clone https://github.com/apertium/apertium-cat
git clone https://github.com/apertium/apertium-eng
```

Quan les ordres s'hagin executat, hi haurà cinc carpetes noves amb les dades lingüístiques.
