---
title: "Apertium Viewer"
weight: 23
disableToc: true
---
1. Descarga Apertium Viewer y guárdalo en una carpeta: [Apertium Viewer](/files/apertium-viewer.jar)

2. Abre el panel de propiedades del archivo «apertium-viewer.jar», y marca el archivo como ejecutable.
