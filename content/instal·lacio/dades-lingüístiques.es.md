---
title: "Datos lingüísticos"
weight: 24
disableToc: true
---

Para descargar los datos lingüísticos de un par de Apertium solo hay que conocer las URL del paquete bilingüe y de los dos paquetes monolingües y tener instalado Git en el ordenador.

Por ejemplo, para descargar los pares castellano-catalán e inglés-catalán:

1. Abre un terminal en una carpeta de trabajo (por ejemplo, Documentos/apertium).

2. Ejecuta las órdenes siguientes:
```sh
git clone https://github.com/apertium/apertium-spa-cat
git clone https://github.com/apertium/apertium-eng-cat
git clone https://github.com/apertium/apertium-spa
git clone https://github.com/apertium/apertium-cat
git clone https://github.com/apertium/apertium-eng
```

Cuando las órdenes se hayan ejecutado, habrá cinco carpetas nuevas con los datos lingüísticos.
