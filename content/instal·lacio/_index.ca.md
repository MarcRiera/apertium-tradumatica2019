---
title: "Apertium: instal·lació"
weight: 30
pre: "<b>3. </b>"
chapter: true
---
### Capítol 3

# Apertium: instal·lació

Aprèn a instal·lar Apertium per al desenvolupament.
