---
title: "Programa base (Debian/Ubuntu)"
weight: 21
disableToc: true
---

Per instal·lar Apertium en un sistema operatiu basat en Debian o Ubuntu, el procediment és molt senzill:

1. Obre el terminal, escriu l'ordre següent, prem Intro i segueix les instruccions en pantalla:
```sh
wget https://apertium.projectjj.com/apt/install-nightly.sh -O - | sudo bash
```

2. Instal·la el paquet **apertium-all-dev** des del gestor de paquets o el terminal:
```sh
sudo apt-get install apertium-all-dev
```
