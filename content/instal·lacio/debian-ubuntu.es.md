---
title: "Programa base (Debian/Ubuntu)"
weight: 21
disableToc: true
---

Para instalar Apertium en un sistema operativo basado en Debian o Ubuntu, el procedimiento es muy sencillo:

1. Abre el terminal, escribe la orden siguiente, pulsa Intro y sigue las instrucciones en pantalla:
```sh
wget https://apertium.projectjj.com/apt/install-nightly.sh -O - | sudo bash
```

2. Instala el paquete **apertium-all-dev** desde el gestor de paquetes o el terminal:
```sh
sudo apt-get install apertium-all-dev
```
