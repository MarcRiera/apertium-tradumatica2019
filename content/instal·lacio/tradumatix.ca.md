---
title: "Programa base (Tradumàtix 19)"
weight: 22
disableToc: true
---

En el cas de Tradumàtix, que en la versió instal·lada als ordinadors de la UAB presenta restriccions, el procediment és una mica diferent a l'habitual.

1. Obre el terminal, escriu l'ordre següent i prem Intro:
```sh
sudo synaptic-pkexec
```

2. Baixa la clau d'Apertium i desa-la en una carpeta: https://apertium.projectjj.com/apt/apertium-packaging.public.gpg

3. A Synaptic, a «Fonts de programari», importa la clau que t'has baixat i el repositori d'Apertium:
```xml
deb http://apertium.projectjj.com/apt/nightly bionic main
```

4. Actualitza la llista de paquets amb el botó superior esquerre de Synaptic.

5. Cerca els paquets **apertium-all-dev** i **git** i marca'ls per instal·lar. Aplica els canvis.
