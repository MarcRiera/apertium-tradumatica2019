---
title: "Apertium Viewer"
weight: 23
disableToc: true
---
1. Baixa Apertium Viewer i desa'l en una carpeta: [Apertium Viewer](/files/apertium-viewer.jar)

2. Obre el panell de propietats del fitxer «apertium-viewer.jar», i marca el fitxer com a executable.
