---
title: "Programa base (Tradumàtix 19)"
weight: 22
disableToc: true
---

En el caso de Tradumàtix, que en la versión instalada en los ordenadores de la UAB presenta restricciones, el procedimiento es un poco diferente al habitual.

1. Abre el terminal, escribe la orden siguiente y pulsa Intro:
```sh
sudo synaptic-pkexec
```

2. Descarga la clave de Apertium y guárdala en una carpeta: https://apertium.projectjj.com/apt/apertium-packaging.public.gpg

3. En Synaptic, en «Fonts de programari», importa la clave que has descargado y el repositorio de Apertium:
```xml
deb http://apertium.projectjj.com/apt/nightly bionic main
```

4. Actualiza la lista de paquetes con el botón superior izquierdo de Synaptic.

5. Busca los paquetes **apertium-all-dev** y **git** y márcalos para instalar. Aplica los cambios.
