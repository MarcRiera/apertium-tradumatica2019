---
title: "¿Qué es el software libre?"
weight: 12
disableToc: true
---

Un software es software libre si los usuarios del programa tienen las cuatro libertades esenciales:

- La libertad de ejecutar el software cuando quieran, con cualquier propósito (libertad 0).

- La libertad de estudiar cómo funciona el software, y modificarlo para que funcione cómo quieran (libertad 1). El acceso al código fuente es un prerrequisito para ello.

- La libertad de redistribuir copias para ayudar a los demás (libertad 2).

- La libertad de distribuir copias de sus versiones modificadas a los demás (libertad 3). Haciendo esto pueden dar a toda la comunidad la oportunidad de beneficiarse de sus cambios. El acceso al código fuente es un prerrequisito para ello.
