---
title: "Què és el programari lliure?"
weight: 12
disableToc: true
---

Un programari és programari lliure si els usuaris del programa tenen les quatre llibertats essencials:

- La llibertat d'executar el programari quan vulguin, amb qualsevol propòsit (llibertat 0).

- La llibertat d'estudiar com funciona el programari, i modificar-lo perquè funcioni com vulguin (llibertat 1). L'accés al codi font és un prerequisit per a això.

- La llibertat de redistribuir còpies per ajudar als altres (llibertat 2).

- La llibertat de distribuir còpies de les seves versions modificades als altres (llibertat 3). Fent això poden donar a tota la comunitat l'oportunitat de beneficiar-se dels seus canvis. L'accés al codi font és un prerequisit per a això.
