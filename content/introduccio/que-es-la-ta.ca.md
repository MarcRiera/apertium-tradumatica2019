---
title: "Què és la traducció automàtica?"
weight: 11
disableToc: true
---

Segons Ginestí-Rosell i Forcada (2009, p. 43):

«La traducció automàtica (TA) és el procés de traducció, mitjançant un sistema informàtic (compost per ordinadors i programes), de textos informatitzats escrits en la llengua origen a textos informatitzats escrits en la llengua meta.»

Depenent de l'ús que se'n vulgui fer, podem distingir entre dos usos de la TA:

- **Assimilació:** la TA serveix com a mitjà per obtenir una idea general d’un text en una llengua desconeguda. Es tracta d’un ús immediat i efímer (les traduccions no es conserven per a més endavant), per la qual cosa la transmissió del sentit del text és més important que els errors en la traducció.

- **Disseminació:** la TA és un pas previ a una revisió (postedició) de la traducció per part d’un especialista. Permet alliberar el traductor de la part més mecànica de la traducció i, en alguns casos, incrementar-ne la productivitat.

Des dels seus inicis, la TA ha anat evolucionant, i actualment podem classificar els sistemes de TA en dos grans grups:

- **Traducció automàtica basada en regles:** són sistemes basats en dades lingüístiques codificades electrònicament en diccionaris, regles gramaticals, etc. És la tecnologia més tradicional, i requereix d’un gran esforç de desenvolupament, però funciona especialment bé entre llengües properes i llengües amb pocs recursos. Dins d’aquest grup, els sistemes es poden classificar en tres categories (Hutchins i Somers, 1992): sistemes directes, sistemes d’interlingua i sistemes de transferència.

- **Traducció automàtica basada en corpus:** són sistemes basats en grans volums de text alineat. És una tecnologia més recent i la més habitual actualment, però requereix corpus alineats amb grans quantitats de text. Dins d'aquest grup, trobem els sistemes basats en exemples, els estadístics i els neuronals.
