---
title: "¿Qué es la traducción automática?"
weight: 11
disableToc: true
---

Según Ginestí-Rosell y Forcada (2009, p. 43):

«La traducción automática (TA) es el proceso de traducción, mediante un sistema informático (compuesto por ordenadores y programas), de textos informatizados escritos en la lengua origen a textos informatizados escritos en la lengua meta.»

Dependiendo del uso que se quiera hacer de ella, podemos distinguir entre dos usos de la TA:

- **Asimilación:** la TA sirve como medio para obtener una idea general de un texto en una lengua desconocida. Se trata de un uso inmediato y efímero (las traducciones no se conservan para más adelante), por lo que la transmisión del sentido del texto es más importante que los errores en la traducción.

- **Diseminación:** la TA es un paso previo a una revisión (posedición) de la traducción por parte de un especialista. Permite liberar al traductor de la parte más mecánica de la traducción y, en algunos casos, incrementar su productividad.

Desde sus inicios, la TA ha ido evolucionando, y actualmente podemos clasificar los sistemas de TA en dos grandes grupos:

- **Traducción automática basada en reglas:** son sistemas basados en datos lingüísticos codificados electrónicamente en diccionarios, reglas gramaticales, etc. Es la tecnología más tradicional, y requiere de un gran esfuerzo de desarrollo, pero funciona especialmente bien entre lenguas cercanas y lenguas con pocos recursos. Dentro de este grupo, los sistemas se pueden clasificar en tres categorías (Hutchins y Somers, 1992): sistemas directos, sistemas de interlingua y sistemas de transferencia.

- **Traducción automática basada en corpus:** son sistemas basados en grandes volúmenes de texto alineado. Es una tecnología más reciente y la más habitual actualmente, pero requiere corpus alineados con grandes cantidades de texto. Dentro de este grupo, encontramos los sistemas basados en ejemplos, los estadísticos y los neuronales.
