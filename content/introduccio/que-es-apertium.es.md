---
title: "¿Qué es Apertium?"
weight: 13
disableToc: true
---

Apertium es una plataforma de traducción automática basada en reglas. Es libre y de código abierto, por lo que cualquier persona que quiera puede usarla y contribuir. Se puede utilizar en línea a través de un sitio web (https://www.apertium.org), o bien instalándola en el ordenador o en el móvil (sin conexión).

Apertium nace después de que en 2004 el Estado español abriera una convocatoria de propuestas para crear sistemas de traducción automática entre las lenguas del Estado. No se creó desde cero, sino que se aprovechó código existente pero de código cerrado.

En 2005 había 3 pares de lenguas disponibles (catalán-castellano, gallego-castellano y portugués-castellano). En 2010 ya eran 27, y actualmente (2019) hay 49. Muchos de los pares incluyen lenguas minoritarias y minorizadas, puesto que la traducción automática basada en reglas es ideal para las lenguas con pocos recursos.

A pesar de que en un principio se centraba en lenguas próximas, las mejoras han permitido que también se puedan construir pares de lenguas más lejanas.
